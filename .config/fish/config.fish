if status is-interactive
    # Connection shorcuts 
    fish_add_path /home/eric/Documentos/Conexion/scripts
end

# Alias
alias vi='nvim'
alias vim='nvim'

# Disable greetings message
set -U fish_greeting

# Language variables
set -Ux MUSL_LOCPATH /usr/share/i18n/locales/musl
set -Ux CHARSET es_ES.UTF-8
set -Ux LANG es_ES.UTF-8
set -Ux LC_COLLATE es_ES.UTF-8
set -Ux LC_ALL es_ES.UTF-8
